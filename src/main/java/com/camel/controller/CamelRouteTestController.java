package com.camel.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;

@RestController
public class CamelRouteTestController {

    Logger LOGGGER = LoggerFactory.getLogger(CamelRouteTestController.class);

    @GetMapping(value = "/test/{message}")
    public void testCamelRoute(@PathParam("message") String message) {
        LOGGGER.info("Send Message is :: {} " ,message);
    }

    @GetMapping(value = "/result/{message}")
    public void resultCamelRoute(@PathParam("message") String message) {
        LOGGGER.info("Received Message is :: {} " ,message);
    }
}

package com.camel.routes;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.slf4j.ILoggerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class JmsTestRouter extends RouteBuilder {

    private Logger LOGGER = LoggerFactory.getLogger(JmsTestRouter.class);
    @Override
    public void configure() throws Exception {

        from("{{input.queue}}")
                .log(LoggingLevel.INFO, log, "New message received")
                .process(exchange -> {
                    String convertedMessage = exchange.getMessage().getBody() + " is converted";
                    exchange.getMessage().setBody(convertedMessage);
                })
                .to("{{output.queue}}")
                .log(LoggingLevel.INFO, log, "Message sent to the reply queue")
        .end();

        restConfiguration()
                .component("servlet")
                .bindingMode(RestBindingMode.auto);

        rest().get("/hello")
                .to("direct:hello");

        from("direct:hello")
                .log(LoggingLevel.INFO, "Get REST Call to Hello")
                .transform().simple("Rest Call Happen Successfully")
                .to("direct:greeting");

        from("direct:greeting")
                .process( exchange -> {
                    LOGGER.info("Greetings :: {}" ,exchange.getMessage().getBody());
                });

        rest("/camel").get("/test/{message}")
                .route().transform().simple("Hi There")
                .endRest()
                .get("/result/{message}").to("direct:greeting");

    }
}
